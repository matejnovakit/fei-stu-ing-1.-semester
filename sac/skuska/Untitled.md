<h1>Okruhy otazok zo SAC</h1>

<i>Z prvých 4 prednášok (IH)</i>

1. <b>Definujte pojem snímač, zvoľte si kritérium a urobte klasifikáciu snímačov</b>
Snímač je zariadenie, ktoré dostáva podnety (alebo impulzy) a odpovedá na ne  elektrickým signálom, kde podnet je buď meraná (fyzikálna) veličina, vlastnosť alebo podmienka, ktorá je snímačom zaznamenaná a ďalej prevedená na elektrický signál. Tento elektrický signál je možné ďalej spracovať nejakým elektronickým zariadením.
Jedným zo základných rozdelení snímačov, je rozdelenie podľa výstupného elektrického signálu, a to na:
- Aktívne – pri pôsobení vstupnej veličiny sa správajú, ako zdroj energie (napr. fotoelektrický snímač)

- Pasívne – pri pôsobení vstupnej veličiny sa mení niektorý parameter snímača napr. odpor (odporový snímač)

2. <b>Aký je význam a potreba SAČ pre náš každodenný život?</b>
Snímač je zariadenie, ktorého účelom je sledovať stav (zmeny) požadovanej fyzikálnej veličiny a transformovať jej hodnotu tak aby sa dala spracovať a vyhodnotiť pripojeným elektrickým obvodom (najčastejšie). Akčný člen je možné uviesť ako opak snímača, keďže vstupný riadiaci signál (elektrický, pneumatický, hydraulický,..), prevádza napríklad na pohyb alebo teplo. Tieto dve zariadenia nám umožňujú spracovať a ovplyvňovať analógový svet okolo nás digitálnymi zariadeniami.

3. <b>Popíšte systém na zber dát a úlohu SAČ v ňom.</b>


4. <b>Vysvetlite paralely a motivácie pre využitie poznatkov z mikroelektroniky a ich prenos do oblasti SAČ.</b>
Motivácia využitia mikroelektroniky v oblasti SAČ spočíva v možnosti znižovania veľkostí zariadení snímačov, ktoré aj vďaka tomu môžu preukazovať efektívnejšie vlastnosti napríklad v oblasti spotreby. Taktiež je veľkou výhodou možnosť prenosu digitálne navrhnutých motívov na substrát optickou cestou, ktorá sa v mikroelektronike používa, čím sa zabezpečí precíznejšie vytvorený snímač. V dnešnej dobe by sme už neboli schopný navrhovať tak malé obvody manuálne, preto oblasť mikroelektroniky zohráva kľúčovú rolu v oblasti návrhu, optimalizácii a simulácii obvodu. Presnosť samotných motívov je limitovaná len vlnovou dĺžkou svetla.

5. <b>Aké sú odlišnosti a problémy medzi mikroelektronikou a oblasťou SAČ a vysvetlite ich možnosti riešenia.</b>
Mikroelektronika potrebuje pre svoju správnu činnosť prijímať informácie z vonkajšieho sveta.
Preto sú tu snímače (senzory), ktoré sprostredkúvajú komunikáciu medzi rôznymi fyzikálnymi podnetmi okolo nás a elektrickými obvodmi.
Snímače predstavujú oči, uši a nosy pre kremíkové čipy, prijímajú fyzikálne podnety a odpovedajú na neho elektrickým signálom.
Problém medzi snímačom a elektronikou je spraviť správnu konverziu medzi vstupným a výstupným signálom. Kým snímač vyprodukuje elektrický signál musí spraviť niekoľko konverzných krokov.
Ideálny vzťah medzi výstupom a podnetom je charakterizovaný prevodovou funkciou (charakteristikou), ktorá môže byť lineárna, logaritmická, exponenciálna, mocninová.. Ak žiadna z aproximácií nezodpovedá skutočnosti, tak sa využívajú polynomické aproximácie vyššieho stupňa.
Ďalším problémom je presnosť, kalibrácia, chyba kalibrácie, zmeny v budení snímača, odpoveď snímača s dynamickou chybou, fázové posunutie výstupného signálu.

6. <b>Aké sú statické parametre snímačov a jeden vybraný parameter podrobnejšie popíšte.</b>
Definícia statických parametrov: Vlastnosti systému po tom, čo sa všetky prechodové javy ustália do ich konečného, alebo ustáleného stavu.
Niektoré zo statických parametrov snímača :
- Citlivosť  
- prah citlivosti  
- dynamický rozsah  
- Reprodukovateľnosť  
- aditívne a multiplikatívne chyby  
- chyba linearity ( nelinearita )  
- chyba hysterézy ( hysteréza )
Citlivosť : 
- je jedným zo základných parametrov snímačov
- Definujeme ju zo statickej prevodovej charakteristiky, pričom vstupnou veličinou je X a výstupnou Y časovo ustálenom stave
- Závislosť snímača vieme popísať polynómom : $y=a_0 + a_1 * x + a_2 * x^2 +...+ a_n * x^n$
- Najdôležitejšou časťou pre snímače sú prvé zložky polynómu, zároveň uvažujeme, že charakteristika začína v nule, preto prvý člen $a_0=0$
- Následne sa dostávame ku vzťahu : $y=K*x$, kde K je citlivosť systému

7. <b>Čo je to prevodová charakteristika snímača, maximálny rozsah vstupných podnetov a maximálny rozsah výstupných elektrických signálov?</b>

8. <b>Vysvetlite vlastnosti snímačov: presnosť, kalibrácia a chyba kalibrácie.</b>

9. <b>Aký význam má výstupná impedancia snímačov pre ďalšie spracovanie výstupných elektrických signálov?</b>

10. <b>Vysvetlite potrebu poznania dynamických charakteristík snímačov.</b>

11. <b>Aké závislosti elektrického odporu využívame v snímačoch? Uveďte jednu závislosť a charakterizujte ju.</b>

12. <b>Aké závislosti elektrickej kapacity môžeme využiť v snímačoch? Podrobnejšie popíšte niektorú z nich.</b>

13. <b>Opíšte špecifické problémy chemických snímačov a ich klasifikáciu</b>

14. <b>Snímače plynu vodivostného typu: princíp detekcie, náčrt štruktúry a používané plynovo-citlivé materiály.</b>

15. <b>Charakterizujte štruktúru a vlastnosti chemo-kondenzátorov, chemo-diód a chemo-tranzistorov.</b>

16. <b>Popíšte mikromechanické komponenty využívané v mechanických snímačoch a ich charakteristické statické a dynamické vlastnosti.</b>

17. <b>Piezoodporové tlakové snímače – štruktúra, princíp vyhodnocovania, vlastnosti.</b>

18. <b>Kapacitné tlakové snímače - štruktúra, princíp vyhodnocovania, vlastnosti.</b>

19. <b>Piezoodporové snímače zrýchlenia: štruktúra, princíp vyhodnocovania, vlastnosti.</b>

20. <b>Kapacitné snímače zrýchlenia: štruktúra, princíp vyhodnocovania, vlastnosti.</b>