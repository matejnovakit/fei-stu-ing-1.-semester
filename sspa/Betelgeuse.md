## Solárna konštanta:
$$S=\frac{4{\pi}R^2{\sigma}T^4}{4{\pi}r^2}$$

#### Po úprave:
$$S=\frac{R^2{\sigma}T^4}{r^2}$$

#### Teplota Slnka:
$$T=[\frac{r^2}{R^2}x\frac{S}{\sigma}]^{1/4}$$

## Hodnoty:
#### Vzdialenost Zem - Slnko:
$$
r=1*5x10^8 km
$$

#### Polomer Slnka:
$$
R=7x10^5 km
$$

#### Solarna konstanta:
$$
S=1*4x10^3 \frac{kW}{m^{2}}
$$

#### Stefan-Boltzmanova konstanta:
$$
{\sigma}=5*67x10^{-8} \frac{W}{m^2K^4}
$$

## Výpočet:
$$T_{Sun}=[\frac{(1*5x10^{8})^2}{(7x10^5)^2}x\frac{1*4x10^3}{5*67x10^{-8}}]^{1/4}$$
$$T_{Sun}=5800 K$$

## Hodnota z internetu:
$$T_{Sun}=5,778 K$$