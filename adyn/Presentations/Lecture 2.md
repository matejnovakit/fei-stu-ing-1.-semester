<h1>Two-body Problem – Motion in Inertial Frame</h1>

<b>Content:</b>
- Newton’s Laws of Motion
- Classical Dynamics
- Newton’s Law of Gravity
- Motion in Inertial Frame

<h2>1. Newton’s Laws of Motion</h2>

- Every body remains in its state of rest or of uniform motion in a straight line, unless it  is compelled to change that state by forces impressed upon it
- State of body is defined by its momentum:

$$\vec{p}=m\vec{v}$$

- The rate of change of momentum is proportional to the motive force impressed and is directed along the straight line in which that force is impressed

$$\frac{d\vec{v}}{dt}=\vec{F}$$

- if mass of system is constant

$$\frac{d\vec{p}}{dt}=\frac{d(m\vec{v})}{dt}=m\frac{d(m\vec{v})}{dt}=m\frac{d\vec{v}}{dt}=m\vec{a}=\vec{F}$$

- To every action there is always opposed an equal reaction, that is, the mutual actions exerted by two bodies are always equal and oppositely directed

$$\vec{F}_{21}=-\vec{F}_{12}$$

<h2>2. Classical Dynamics</h2>

- Work
- Energy
- Conversation of total energy
- Angular momentum

<h2>3. Newton's Law of Gravity</h2>

-  Newton’s law of gravity can be expressed by two point masses:
	-  each of the two bodies can be considered as a particle having all its mass concentrated in its centre of mass
	-  each of the two bodies is only subject to the gravitational force produced by the other body and directed along the straight line joining the centres of mass of the two bodies

- Newton’s law of gravity can be expressed by two point masses

![[p2_two_points.png]]

$$\vec{F}_{21}=-\vec{F}_{12}=-G\frac{m_1m_2}{r^3}\vec{r}$$

- Universal gravitational constant (1st time measured by Cavendish, 1798) ([[G_const]])
$$G=6.6742x10^{-11} \frac{m^3}{kgs^2}$$

- conservative force can be expressed by potential energy

$$W_{AB}=\int_{r_A}^{r_B} \vec{F_{12}} \,dt => E_p=-G\frac{m_1m_2}{r}$$

- μ can be measured with considerable precision by astronomical observation

$$\vec{F}_{21}=-\vec{F}_{12}=-\frac{{\mu}m_2}{r^3}\vec{r}$$

![[p2_mu.png]]

$$\vec{F}_{21}=m_2\vec{a}=m_2\frac{Gm_1}{r^3}\vec{r} => \vec{g}=G\frac{m_Z}{r^3}=\vec{g}_0(\frac{R_Z}{R_Z+z})$$

<h2>4. Motion in Inertial Frame</h2>

- Two body problem can by defined by:
	- Newton’s law of gravitation $\vec{F}_{21}=-\vec{F}_{12}=-G\frac{m_1m_2}{r^3}\vec{r}$
	- Newton's laws of motion $m_1\vec{\ddot{R}_1}=\vec{F}_{21} | m_2\vec{\ddot{R}_2}=\vec{F}_{21}$

- Barycenter:

![[p2_barycenter.png]]

$$(m_1+m_2)\vec{R}_B=m_1\vec{R}_1+m_2\vec{R}_2 => \vec{R}_B=\frac{m_1\vec{R}_1+m_2\vec{R}_2}{m_1+m_2}$$

- 2x derivative:

$$\vec{\ddot{R}}_B=\frac{m_1\vec{\ddot{R}}_1+m_2\vec{\ddot{R}}_2}{m_1+m_2}$$

$$\vec{\ddot{R}}_B=0$$

- center of mass is:
	- motionless
	- or motion is in straight line with constant velocity

- then:

$$m_1\vec{\ddot{R}_1}=\vec{F}_{21} => m_1\vec{\ddot{R}_1}=G\frac{m_1m_2}{r^3}\vec{r} => {\ddot{R}_1}=G\frac{m_2}{r^3}\vec{r}$$
$$m_2\vec{\ddot{R}_2}=\vec{F}_{21} => m_2\vec{\ddot{R}_2}=-G\frac{m_1m_2}{r^3}\vec{r} => {\ddot{R}_2}=-G\frac{m_1}{r^3}\vec{r}$$

- and:

$$\vec{r}=\vec{R}_2-\vec{R}_1 => \vec{\ddot{r}}=\vec{\ddot{R}}_2-\vec{\ddot{R}}_1$$

$$\vec{\ddot{r}}=-G\frac{m_1+m_2}{r^3}\vec{r} => \vec{\ddot{r}}+\frac{\mu}{r^3}\vec{r}=0$$

- FINAL:

$$\vec{\ddot{r}}+\frac{\mu}{r^3}\vec{r}=0$$

- gravitational parameter ([[def mu param]]):
	- $\mu=G(m_1+m_2)$
	- $if: m_1 >> m_2$ $then: \mu=Gm_1$	
- initial conditions:
	- $\vec{r}(0)=\vec{r}_0$
	- $\vec{\dot{r}}(0)=\vec{v}_0$

- vector $\vec{r}$ is defined in inertial frame as:

$$\vec{r}=(x_2-x_1)\vec{i}+(y_2-y_1)\vec{j}+(z_2-z_1)\vec{k}$$



