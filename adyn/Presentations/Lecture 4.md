<h1>Two body problem in 2D I.</h1>

<b>Content:</b>
- Trajectories
- Circular trajectory
- Elliptical trajectory
- Parabolic trajectory
- Hyperbolic trajectory

<h2>1. Trajectories</h2>

- Tvar trajektorie zavisi od ekcentricity e
![[cone-section.png]]
- Vzorce orbity: 
	- trajektoria: $$r=\frac{h^2}{\mu}\frac{1}{(1+ecos\theta)}$$
	- rychlost: $$v=\frac{\mu}{h}\sqrt{(1+e^2+2ecos\theta)}$$
	- energia:  $$\varepsilon=\frac{v^2}{2}-\frac{\mu}{r}=-\frac{1}{2}\frac{\mu^2}{h^2}(1-e^2)$$


<h2>2. Circular trajectory</h2>

-  e=0
- rovnica orbity: $$r=\frac{h^2}{\mu}\frac{1}{(1+ecos\theta)} => r=\frac{h^2}{\mu}$$
- rovnica rychlosti: $$v=\frac{\mu}{h}\sqrt{(1+e^2+2ecos\theta)} => v=\frac{\mu}{h} => v=\sqrt{\frac{\mu}{r}}$$
- rovnica specifickej energie: $$\varepsilon=-\frac{1}{2}\frac{\mu^2}{h^2}(1-e^2) => \varepsilon=-\frac{1}{2}\frac{\mu^2}{h^2} =>\varepsilon=-\frac{1}{2}\frac{\mu}{r}$$
- rovnica periody obehu: $$T=\frac{2\pi r}{\sqrt{\mu r}}=\frac{2 \pi}{\mu}r^{3/2}$$
![[kruznica.png]]
![[trajektorie.png]]
- orbitalna rychlost=1.kozmicka rychlost

<h2>3. Elliptical trajectory</h2>

- 0<e<1
- rychlost:
- obr. page 13
- periapsida - najblizsi bod k telesu - P
- apoapsida - najvzdialenejsi bod - A
- vzdialenost ku kazdemu bodu na elipse je rovne 2*a
- perioda je vzdy rovnaka
- rovnica orbity: $$r=\frac{h^2}{\mu}\frac{1}{(1+ecos\theta)}$$
$$r_P=\frac{h^2}{\mu}\frac{1}{(1+e)}$$ 
$$r_A=\frac{h^2}{\mu}\frac{1}{(1-e)}$$ $$\frac{r_P}{r_A}=\frac{1-e}{1+e} => e=\frac{r_A-r_P}{r_A+r_P}=\frac{2c}{2a}=\frac{c}{a}$$
![[p4_ellipse.png]]

- semimajor axis <b>a</b> & semiminor axis <b>b</b>:
$$a=\frac{1}{2}(r_A+r_P)$$
$$a=\frac{1}{2}\frac{h^2}{\mu}(\frac{1}{1+e}+\frac{1}{1-e})=\frac{h^2}{2\mu}(\frac{1-e+1+e}{(1+e)(1-e)})=\frac{h^2}{2\mu}\frac{2}{(1-e^2)}=\frac{h^2}{\mu}\frac{1}{(1-e^2)}$$

$$e=\frac{c}{a} => b=\sqrt{a^2-c^2} => b=a\sqrt{(1-e^2)}$$
$$a=\frac{h^2}{\mu}\frac{1}{(1-e^2)}$$
$$b=\frac{h^2}{\mu}\frac{1}{\sqrt{(1-e^2)}}$$

- eccentricity
$$f=\frac{a-b}{a} <= flattening (splostenie Zeme)$$
$$e^2=\frac{a^2-b^2}{a^2} => f=1-\sqrt{1-e^2}$$
![[p4_e_f.png]]

- equation of orbit:
$$r=\frac{h^2}{\mu}\frac{1}{(1+ecos\theta)}$$
- specific energy:
$$\varepsilon=-\frac{1}{2}\frac{\mu^2}{h^2}(1-e^2)=-\frac{1}{2}\frac{\mu}{h}$$
- speed of motion:
$$\varepsilon=\frac{v^2}{2}-\frac{\mu}{r}$$
$$v=\sqrt{-\frac{\mu}{a}+\frac{2\mu}{r}}$$
- period of orbit:
$$T=\frac{2\pi a^{3/2}}{\sqrt{\mu}}$$
- Kepler 3. law: $\frac{T^2}{a^3}=\frac{4\pi^2}{\mu}$

<h2>4. Parabolic trajectory</h2>

- e=1
- equation of orbit: 

$$r=\frac{h^2}{\mu}\frac{1}{1+ecos{\theta}} => r=\frac{h^2}{\mu}\frac{1}{1+cos\theta}$$

- specific energy:
$$\varepsilon=-\frac{1}{2}\frac{\mu^2}{h^2}(1-e^2)=>\varepsilon=0$$
- speed of motion: 
$$\varepsilon=\frac{v^2}{2}-\frac{\mu}{r} => v=\sqrt{\frac{2u}{r}}$$

- it's open trajectory => e=1 $$v=\sqrt{\frac{2\mu}{r}}$$
![[parabolic.png]]
- Earth have escape velocity (2. cosmic velocity) = 11.18 km/s

<h2>Hyperbolic trajectory</h2>

- e>1
- equation of orbit: 

$$r=\frac{h^2}{\mu}\frac{1}{1+ecos{\theta}} => \theta_\infty=arccos(-\frac{1}{e} => \beta=\pi-\theta_\infty => \delta=\pi-2\beta$$
 $$r_p=\frac{h^2}{\mu}\frac{1}{(1+e)}$$
 $$r_a=\frac{h^2}{\mu}{1}{1-e}$$
 $$a=\frac{|r_A|-r_p}{2}=\frac{h^2}{2\mu}(\frac{1}{1-e}+\frac{1}{1+e})=\frac{1+e+1-e}{(1-e)(1+e)} => a=\frac{h^2}{\mu}\frac{1}{e^2-1}$$
 specific energy: $$\varepsilon=-\frac{1}{2}\frac{\mu^2}{h^2}(1-e^2) => \varepsilon=\frac{1}{2}\frac{\mu}{a}$$
 speed of motion: $$\varepsilon=\frac{v^2}{2}-\frac{\mu}{r}$$
 
![[hyperbolic.png]]
- [sphere of influence](https://en.wikipedia.org/wiki/Sphere_of_influence) - vplyv napr. Zeme na satelit pokial je dost blizko, nasledne, ked satelit opusti tuto sphere, dostav sa do vplyvu dalsieho telesa, ktore je blizko. Nasledne, ked sme blizko nasej cielovej planety, musime znizit rychlost a vytvorit elipticku drahu na korektne pristatie
- [flyby](https://en.wikipedia.org/wiki/Flyby_(spaceflight)) / gravitacny prak - vyuzitie gravitacie blizkeho telesa na zrychlenie satelitu
- patch cones - upravovanie trajektorie

![[heperbolic2.png]]

![[hyperbola3.png]]

[sunsynchronous orbit](https://en.wikipedia.org/wiki/Sun-synchronous_orbit)
