<h1>Two body problem in 2D II.</h1>

<b>Content:</b>
- Time and position
- Time and position in Circular trajectory
- Time and position in Elliptical trajectory

<h2>1. Time and position</h2>

- Two problems:
	- at what time will be satellite in defined position (t is defined, where is satellite?)
	- where will be satellite at defined time 

- Equation, where is explicity time: $$h=rv_\theta={\theta}r^2 => h=\frac{d\theta}{dt}r^2 => dt=\frac{r^2}{h}d\theta => dt=\frac{h^3}{\mu^2}\frac{1}{(1+ecos\theta)^2}d\theta$$
$$t-t_p=\frac{h^3}{\mu^2}\int_{0}^{\theta}\frac{1}{(1+ecos\theta)^2}$$
- $t_P=t_0=0$
$$t=\frac{h^3}{\mu^2}\int_{0}^{\theta}\frac{1}{(1+ecos\theta)^2}$$

<h2>2. Circular trajectory</h2>

- e=0
$$r=\frac{h^2}{\mu}$$
$$T=\frac{2{\pi}r}{\sqrt{\frac{\mu}{r}}}=\frac{2\pi}{\sqrt{\mu}}r^{\frac{3}{2}}$$
$$t=\frac{r^{\frac{3}{2}}}{\sqrt{\mu}}\theta=\frac{T}{2\pi}\theta$$
![[time.png]]

<h2>3. Elliptical trajectory</h2>

- true anomaly - su to pomocne <b>uhly</b>
- excentric anomaly - premietnuta kruznica okolo elipsy
- mean anomaly - stredna anomalia: satelit by sa pohyboval po fiktivnej kruznici
![[time2.png]]
![[time3.png]]

SKIPPED <220->274>