<h1>Orbits in 3D</h1>

<b>Content:</b>
- Reference frame and reference system
- Reference systems of Earth
- Orbital elements
- Examples of 3D orbits

<h2>1. Reference frame and reference system</h2>

- to describe orbits in three dimensions, the coordinate system in frame of reference must be defined
- Newtons laws are valid in intertial frame of reference }(inercialna vztazna sustava - taka, ze tam platia Newtonove zakony - 1. Newtonov zakon)
- only pseudoinertial frame of reference can be considered
- coordinate system is formed in considered frame of reference

![[p8_inertialFrame.png]]

- reference system - theoretical definition of reference directions of unit vectors
- reference frame - practical construction of reference system
- examples:
	- ICRS - [International Celestial Reference System](https://en.wikipedia.org/wiki/International_Celestial_Reference_System)
	- ICFS - [International Celestial Reference Frame](https://en.wikipedia.org/wiki/International_Celestial_Reference_Frame)

- [Ecliptic](https://en.wikipedia.org/wiki/Ecliptic)
- [Precesny pohyb](https://sk.wikipedia.org/wiki/Precesia_(astron%C3%B3mia)) ~25800 rokov.

![[p8_reference.png]]

- <i>vsetky inercialny sustavy je nutne prepocitavat do Topocentrickej vztaznej sustavy</i>
- using coordinate system can define position in reference frame 

![[p8_2dvs3d.png]]

- <b>Time</b> - "the main purpose of time si to define with precision the moment of a phenomenon" (Newcomb)
- This moment is called epoch of the event
- Time is a fundamental dimension in almost every branch of science
- To have a practical time system, we need a precise, repeatable time interval based on some physical phenomenon that we can readily measure
- We must agree on a fundamental epoch from which to count intervals
- Four time scale now provide timekeeping for scientific, engineering and general purposes:
	- Solar time (bezne pouzivame) - trva 24h
	- Sidereal time (hviezdny den)  - trva ~ 23h 56m 4.090524s solar time
	- Univerzal time - based on a fictitious mean Sun exhibiting uniform motion in right ascension along the equator. Cas sa vztahuje na "fiktivne stredove Slnko"
	- Dynamical time 
	- Atomic time - most precise time standard. Based on the specific quantum transition of electrons in a cesium-133 atom.

- Sidereal year = 365.2596 days
- Tropical year = 365.2422 days
- Gregorian calendar = 365.2425 days (enhanced)
- [Julian day number](https://en.wikipedia.org/wiki/Julian_day)

![[p8_solar_sidereal_day.png]]

- [meridian](https://en.wikipedia.org/wiki/Meridian_(astronomy))
- Solar time is loosely defined by successive transits of the Sun over a local meridian.
- Sidereal time is defined as the time between successive transits of the stars over a particular meridian
- Irregularities in the Sun’s apparent motion make it difficult to use for reckoning time. As a result, the concept of universal time, UT, was adopted years ago
- UT is based on a fictitious mean Sun exhibiting uniform motion in right ascension along the equator
- We can also measure time by the motion of bodies, such as the Earth’s motion about the Sun or, more usually, the Moon’s motion about the Earth.
- This is dynamical time, which depends on the fact that time is the independent variable in the equations of motion describing the object’s motion
- Atomic time is the most precise time standard. It’s based on the specific quantum transition of electrons in a cesium‐133 atom.
- The transition causes the emission of photons of a known frequency that we can count. We define the atomic second by a fixed number of cycles.
- Solar time is based on the interval between successive transits of the Sun over a local meridian, which establishes the solar day.
- The Sun’s apparent motion results from a combination of the Earth’s rotation on its axis and its annual orbital motion about the Sun.
- The Earth moves with a variable speed in the orbit, as shown in Kepler’s second law.
- These factors affect the Sun’s apparent annual motion in the sky, causing the Sun to exhibit nonuniform motion along the ecliptic
- The Earth’s orbit about the Sun has a small eccentricity, causing the length of each day to differ by a small amount. Apparent solar time is the interval between successive transits which we observe from a particular longitude
- A Fictitious Mean Sun was proposed by Simon Newcomb in 1895. He defined it to have nearly uniform motion along the celestial equator.
- We define the mean solar time at Greenwich as universal time

<h2>2. Reference systems of Earth</h2>

- GES - Geocentric Equatorial System / ECI Earth-Centered Inertial system
- ECEF - Earth-Centered, Earth-Fixed
- Topocentric Horizon Coord. System
- Perifocal Coord. System

ECI

![[p8_eci.png]]
- the center of coord. system is at Earth’s center
- not‐rotating coord. system
- fundamental plane – Earth’s equator plane
- axis X points towards the vernal equinox
- axis Z extends through the North Pole
- equinox points = 12h day/night
- ECI frame of reference is not fixed in space:
	- gravitational forces of planets – planetary precession
	- gravitational forces of Moon and Sun – luni‐solar precession with period 26,000 years
	- combined effect – general precession
	- inclination of Moon – additional torque on Earth’s equatorial bulge – nutation with period 18.6 years
	- due to precession and nutation equinox is moving
- for all precise applications, ECI must by defined on specific date
- J2000 ‐ commonly used ECI frame is defined with the Earth's Mean Equator and Equinox at 12:00 Terrestrial Time on 1 January 2000

ECEF
- the center of coord. system is at Earth’s center
- rotating coord. system
- fundamental plane – Earth’s equator plane
- axis Z extends through the North Pole
![[p6_ecef.png]]

<h2>Orbital elements</h2>

![[p8_inclanation.png]]
Official explanation:
- $\Omega,i$ - the location of the orbital plane in defined coord. system of chosen frame of reference
- $\omega$ - the position of the elliptical orbit in this plane
- $e,h$ - the characteristics of ellipse
- $\theta (or M)$ - the position of the moving satellite on the orbit

My explanation:
- $\Omega$,i = uhol natocenia vektora $\vec{n}$ voci vektoru $\vec{i}$ - ako vyzera robina vzhladom na rovnik
	- i=inklanacia (if=0, rovina je identicka s k)
	- $\Omega$ ako je natocena podla vektora n
- $\omega$ position of the elliptical orgit in this plane - ako je natocena elipsa od vektora $\vec{n}$ (kde je natocena periapsida, najblizsi bod elipsy?)
- e,h (or a) the characteristics of ellipse - aka je ekcentricita elipsy
	- h=moment hybnosti
- $\theta$ the position of the moving satellite on the orbit - kde sa satelit nachadza	

The goal is to determine orbital elements from:
- position vector: $\vec{r}=r_x\vec{i}+r_y\vec{j}+r_z\vec{k}$
- velocity vector: $\vec{v}=v_x\vec{i}+v_y\vec{j}+v_z\vec{k}$
- both vectors are defined in GES at time $t_0$

- state vector $\vec{r}$ and $\vec{v}$ 
	- veliciny popisujuce pociatocne podnmienky satelitu pri jeho analyze

$$\vec{h}=\vec{r}x\vec{v}=DETERMINANYT=\vec{i}(v_yv_z)-\vec{j}(v_xv_z)+\vec{k}(r_xv_y-v_yv_x)=h_x\vec{i}+h_y\vec{j}+h_z\vec{k}$$

velkost h:
$$h=\sqrt{(h_x)^2+(h_y)^2+(h_z)^2}$$

$$\vec{h}=\vec{r}x\vec{v}=\begin{vmatrix}
\vec{i} & \vec{j} & \vec{k} \\
r_x & r_y & r_z \\
v_x & v_y & v_z \\
\end{vmatrix}$$

$$\vec{h}*\vec{k}=h*k*cos(i) => i=arccos\frac{\vec{h}.\vec{k}}{h.k}$$
$$i=arccos\frac{h_z}{h}$$

![[p6_h_i.png]]

- Eccentricity
$$\vec{e}=\frac{1}{\mu}[(v^2-\frac{\mu}{r})\vec{r}-(\vec{r}.\vec{v})\vec{v}]$$

- vector $\vec{n}$:
$$\vec{n}=\vec{k}x\vec{h}=
\begin{vmatrix}
\vec{i} & \vec{j} & \vec{k} \\
0 & 0 & 1 \\
h_x & h_y & h_z \\
\end{vmatrix}$$

$$\Omega=arccos\frac{n_x}{n}$$
$$\omega=arccos(\frac{\vec{n}}{n}.\frac{\vec{e}}{e})$$
$$\theta=arccos(\frac{\vec{e}}{e}.\frac{\vec{r}}{r})$$

- Vectors:
$\vec{r}=r_x\vec{i}+r_y\vec{j}+r_z\vec{k}$ 
$\vec{v}=v_x\vec{i}+v_y\vec{j}+v_z\vec{k}$
$\vec{h}=h_x\vec{i}+h_y\vec{j}+h_z\vec{k}$ < state vector $\vec{r}$ and $\vec{v}$ equals $\vec{h}$
$\vec{e}=e_x\vec{i}+e_y\vec{j}+e_z\vec{k}$ < state vector $\vec{r}$ and $\vec{v}$ equals $\vec{h}$
$\vec{n}=n_x\vec{i}+n_y\vec{j}+0\vec{k}$ < by 1. element $\vec{h}$

- Angles:
$i=arccos\frac{h_z}{h}$
$\Omega=arccos\frac{n_x}{n}$
$\omega=arccos(\frac{\vec{n}}{n}.\frac{\vec{e}}{e})$
$\theta=arccos(\frac{\vec{e}}{e}.\frac{\vec{r}}{r})$

- All together in scheme:
![[p6_elements.png]]

