<h1>Two-body Problem – Relative Motion</h1>

<b>Content:</b>
- Relative Motion of Two Bodies
- Angular Momentum
- Solution of Two-body Problem
- Energy of Two-body System

<h2>1. Relative Motion of Two Bodies</h2>

- Two body problem can by defined by:
	- Newton’s law of gravitation $\vec{\ddot{r}}+\frac{\mu}{r^3}\vec{r}=0$
	- Newton's laws of motion $\vec{\ddot{r}}+\frac{\mu}{r^3}\vec{r}=0$
		- initial conditions:
			- $\vec{r}(0)=\vec{r}_0$
			- $\vec{\dot{r}}(0)=\vec{v}_0$

![[p3_inertial.png]]

- $m_1$ has coords $[x_1,y_1,z_1]$ and $m_2$ has coords $[x_2,y_2,z_2]$
- then, in equation: $\vec{\ddot{r}}+\frac{\mu}{r^3}\vec{r}=0$
- vector r defined in inertial frame of reference expressed in coord. system $\vec{i} \vec{j} \vec{k}$
- So, equation is: $\vec{r}=(x_2-x_1)\vec{i}+(y_2-y_1)\vec{j}+(z_2-z_1)\vec{k}$
- vector r can be expressed in frame of reference $[\vec{i}_1\vec{j}_1\vec{k}_1]$, that rotates about inertial  frame of reference with instant angular velocity <b>ω</b> and instant angular acceleration <b>α</b>: $\vec{r}=(x_{21})\vec{i}_1+(y_{21})\vec{j}_1+(z_{21})\vec{k}_1$

![[p3_noninertial.png]]

- 2x derivacia casu v inertial frame of reference
$\vec{\ddot{r}}=\vec{\ddot{r}_{rel}}+\vec{\alpha} x \vec{r}+\vec{\omega} x (\vec{\omega} x \vec{r})+2\vec{\omega} x \vec{\dot{r}}_{rel}$

- $\vec{\ddot{r}}=\vec{\ddot{r}_{rel}}$ < when $\vec{\omega}$ and $\vec{\alpha}$ are 0
- relative acceleration of moving (non-rotating) frame of reference in coord. components:
$\ddot{x}_{21}=-\frac{\mu}{r^3}x_{21}$
$\ddot{y}_{21}=-\frac{\mu}{r^3}y_{21}$
$\ddot{z}_{21}=-\frac{\mu}{r^3}z_{21}$
- there are 6 initial conditions

<h2>Angular Momentum</h2>

- relative angular momentum of body $m_2$ per unit mass

![[p3_angular.png]]

<h2>Solution of Two-body Problem</h2>

- Equation of orbit: $\vec{\ddot{r}}=-\frac{\mu}{r^3}\vec{r}$

![[p3_orbit.png]]

$\vec{h}=\dot{\theta}r^2\vec{k}_z$
$\vec{r}=r\vec{i}_r$
- cross product with $\vec{h}$:  $\vec{\ddot{r}}x\vec{h}=-\frac{\mu}{r^3}\vec{r}x\vec{h}$
- $\vec{r}$ and $vec{h}$ expressed by polar coordinates
- the result is: $\vec{\ddot{r}}x\vec{h}=\mu\dot{\theta}\vec{j}_{\theta}$
- angular momentum is constant vector: $\frac{d\vec{h}}{dt}=0$
- left side: $\vec{\ddot{r}}x\vec{h}=\frac{d}{dt}(\vec{\dot{r}}x\vec{h})$
- right side: $\mu\dot{\theta}\vec{j}_{\theta}=\mu\frac{d\theta}{dt}\vec{j}_{\theta}$
- together - differential vector equation: $\frac{d}{dt}(\vec{\dot{r}}x\vec{h})=\mu\frac{d\theta}{dt}\vec{j}_{\theta}$
- Unit vectors of polar coord. system are not constant vectors:
	- $\vec{i}_r=cos\theta\vec{i}_1+sin\theta\vec{j}_1$
	- $\vec{j}_{\theta}=-sin\theta\vec{i}_1+cos\theta\vec{j}_1$

- result of this two equations is: $\frac{d\vec{i}_r}{d\theta}=-sin\theta\vec{i}_1+cos\theta\vec{j}_1=\vec{j}_{\theta}$

- $\vec{j}_{\theta}=\frac{d\vec{i}_r}{d\theta}$ can be replaced in $\frac{d}{dt}(\vec{\dot{r}}x\vec{h})=\mu\frac{d\theta}{dt}\vec{j}_{\theta}$ so the result will be:
$\frac{d}{dt}(\vec{\dot{r}}x\vec{h})=\mu\frac{d}{dt}(\vec{i}_r)$
- diferencna rovnica 1. radu vektorova
- integracna konstanta musi byt vektor
- <b>e</b> is integration constant - excentricity, direction $\vec{i},\vec{e}=e\vec{i}_1$, then: $\vec{\dot{r}}x\vec{h}=\mu(\vec{i}_r+\vec{e})$
- dot (. in my opinion)in follow equation is product with <b>r</b>: 
$r.(\vec{\dot{r}}x\vec{h})=\mu\vec{r}.(\vec{i}_r+\vec{e})$

KONIEC NA 81.Slide
