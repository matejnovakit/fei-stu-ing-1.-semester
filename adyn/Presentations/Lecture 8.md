<h1>Orbital Perturbations II.</h1>

<b>Content:</b>
- Geoid
- Equation of motion with perturbations
- Special Perturbation Techniques
- General Perturbation Techniques

<h2>1. Geoid</h2>

Real shape of Earth is not sphere
Position vector can be expressed as:
$$s=x_{1s}\vec{i}_1+y_{1s}\vec{j}_1+z_{1s}\vec{k}_1$$
$$s(cos\beta cos\alpha\vec{i}_1+cos\beta sin\alpha\vec{j}_1+sin\beta\vec{k}_1)$$
![[p8_shape.png]]
position vector $\vec{r}$ can be expressed as:
$$r(cos\psi cos\lambda\vec{i}_1+cos\psi sin\lambda\vec{j}_1+sin\psi\vec{k}_1)$$
![[p8_position.png]]

using scalar product of vectors $\vec{s}$ and $\vec{r}$ we can obtain:
$\vec{s}.\vec{r}=srcos\sigma=s(cos\beta cos\alpha\vec{i}_1+cos\beta sin\alpha\vec{j}_1+sin\beta\vec{k}_1).r(cos\psi cos\lambda\vec{i}_1+cos\psi sin\lambda\vec{j}_1+sin\psi\vec{k}_1)$
$cos\sigma=sin\psi sin\beta+cos\psi cos\beta cos(\lambda-\alpha)$

Geopotential can be expressed as:
$U(\vec{r})=\int_{(M)}dU=\int_{(M)}\frac{G}{p}dM=G\int_{(M)}\frac{1}{r(1-2(\frac{s}{r})cos\sigma+(\frac{s}{r})^2)^\frac{1}{2}}dm$ <- kosinusova veta

Expression 1/p:
$$\frac{1}{p}=\frac{1}{r(1-2(\frac{s}{r})cos\sigma+(\frac{s}{r})^2)^\frac{1}{2}}=\frac{1}{r}\sum_{n=0}^{\inf}(\frac{s}{r})P_n(cos\sigma)$$
$$P_n(x)=\frac{1}{2^nn!}\frac{d^n}{dx^n}(x^2-1)^n$$
- $P_n(cos\sigma)$ is Legendre polynomial of degree n with argument $\sigma$
- $P_n{x}$ is Legendre polynomial
- Legendre polynomials: $P_n(x)=\frac{1}{2^nn!}\frac{d^n}{dx^n}(x^2-1)^n$ <- argument $cos\sigma$
![[p8_lagendre.png]]

Lagendre polynomials of n degree with argument $cos\sigma$
$$cos\sigma=sin\psi sin\beta+cos\psi cos\beta cos(\lambda-\alpha)$$ 
$$P_n(cos\sigma)=P_n(sin\psi)P_n(sin\beta)+\sum_{m=1}^{n}\frac{(n-m)!}{(n+m)!}P_{nm}(sin\psi)P_{nm}(sin\beta)cosm(\lambda-a)$$
$P_{nm}(sin\psi)$ and $P_{nm}(sin\beta)$ are associated Legendre polynomials:
$P_{nm}(x)=(1-x^2)^{m/2}\frac{d^m}{dx^m}P_n(x)$ <- argument $sin\psi$
![[p8_polynomial.png]]

<b>Geopotential expression skipped [432 (12) - 437 (17)] - zapisal som len cnm a snm</b>

$$U(r,\psi,\lambda)=\frac{GM}{r}\sum_{n=0}^{inf}\sum_{m=0}^{n}(\frac{R_Z}{r})^n(C_{nm}cosm\lambda+S_{nm}sinm\lambda)P_{nm}(sin\psi)$$
$$C_{nm}=\frac{2-\delta_{0m}}{M}\frac{(n-m)!}{(n+m)!}\int_{(M)}(\frac{s}{R_Z})^nP_{nm}(sin\beta)cosm\alpha dM$$
$$S_{nm}=\frac{2-\delta_{0m}}{M}\frac{(n-m)!}{(n+m)!}\int_{(M)}(\frac{s}{R_Z})^nP_{nm}(sin\beta)cosm\alpha dM$$
- Coefficients $C_{nm}$ and $S_{nm}$ represents geometric distribution of the Earth's mass
- These coefficients are determined from experimental measurement ‐ based on observing the real orbits of satellites in orbit around the Earth

Complex functions called Spherical Harmonics (created by $C_{nm}$ and $S_{nm}$):
$$H_{nm}(\psi,\lambda)=P_{nm}(sin\psi)e^{im\lambda}$$

- Goepotential can be expressed in from:
$$U(r,\psi,lambda)=\frac{GM}{r}\sum_{n=0}^{inf}\sum_{m=0}^{n}(\frac{R_Z}{r})^n(C_{nm}cosm\lambda+S_{nm}sinm\lambda)P_{nm}(sin\psi)$$ and then
$$H_{nm}(\psi,\lambda)=P_{nm}(sin\psi)e^{im\lambda}$$

Three group of Spherical harmonics functions:
-	zonal harmonics: $m=0, i.e.H_{n0}(\psi,\lambda)=P_{n0}(sin\psi)=P_n(sin\psi)$
	-	these functions change their sign in the direction of the geocentric latitude $\psi$
	-	the polynomial of the n‐th degree changes times this sign
	-	the geocentric longitude $\lambda$ does not occur in the relation
	-	this type of spherical functions divides the sphere into zones, therefore these functions are called zonally
	-	the zonal members of harmonic functions have the greatest influence on the change of individual orbital elements
-	tesseral harmonics:$0<m<n, i.e. H_{nm}(\psi,\lambda)$
	-	with this type of harmonic functions, the sign of the function changes not only depending on the geocentric latitude $\psi$, but also depending on the geocentric longitude $\lambda$
	-	since sin ($m\lambda$) and cos ($m\lambda$) appear in the expressions of harmonic functions, over the entire interval of length $2\pi$ they always have $2m$ transitions from plus to minus and at the same time in the direction of latitude there is a change of signs according to zonal functions, while a tile pattern is created
-	sectoral harmonics: $m=n, i.e. H_{nm}(\psi,\lambda)$
	-	they are called teseral functions, i.e. tile function. Teseral members of harmonic functions have an effect on the so‐called resonance, where the satellite flies continuously nextto a large mountain range, which will increase the impact of this mountain range.
	-	with these functions, degeneration of teser functions occurs by dividing the sphere into individual sectors
	-	sectoral members of harmonic functions affect the geosynchronous orbits of satellites ‐ they move them in the east ‐ west direction

<h2>2. Equation of motion with perturbations</h2>

- Equation of stellite motion with perturbations – conservative forces: $\vec{\ddot{r}}=grad(U)$
- initial condition: 
	- $\vec{r}(0)=\vec{r}_0$
	- $\vec{\dot{r}}(0)=\vec{v}_0$

- The potential U can be divided into the dominant influence potential $U_0$ and the perturbation potential R: $U=U_0+R$
- The dominant influence potential $U_0$ is manifested in the satellite motion by the central acceleration $\vec{a}_c$, and the perturbation potential R is manifested in the satellite motion by the perturbation acceleration $\vec{a}_p$
	- $\vec{a}_c=grad(U_0)$
	- $\vec{a}_p=grad(R)$

- if we consider 2. approx. of Earth - Ellipsoid:
$U_0=\frac{\mu}{r}$
$R=-\frac{\mu R_Z^2}{r^3}J_2(\frac{3sin^2\psi-1}{2})$ <- $U=U_0+R$

- analytical methods: -> general perturbations
	- expresses modification of motion
	- enable to determine whether the eccentricity increases, the orbit begins to precess, and so on
- numerical methods: -> special perturbations
	- one step methods – purely mathematical approach: Runge‐Kuta
	- multistep methods – methods developed by astronomers to determine the motions of planets: Adams‐Bashforth, Adams‐Moulton
	- special methods design specially for artificial satellites

<h2>3. Special perturbation techniques</h2>

- one step methods – purely mathematical approach:
	- Runge‐Kuta
- multistep methods – methods developed by astronomers to determine the motions of planets:
	- Adams‐Bashforth
	- Adams‐Moulton
- special methods design specially for artificial satellites

<h2>4. General perturbation techniques</h2>

- Variation of parameters is analytical method to investigate influence of perturbation on planetary or satellite motion
- Mathematical intro:
	- diff. equation with right hand side: 
	$\frac{dy}{dt}+f(t)y=g(t)$ => $\frac{dy}{dt}+f(t)y=0$ => $\frac{dy}{y}=-f(t)dt$ => $y=ce^{-\int f(t)dt}$ 
		- homogenous solution c – int. constant
		- to obtain solution of eq. with right hand side, we allow c to be function of t
	$\frac{dc}{dt}e^{-\int f(t)dt}=g(t)$ => $c(t)=C+\int{g(t)e^{\int{f(t)dt}}dt}$
- Similar process can be applied to system of diff. eq.
- diff. equation of motion can be written as system of equations:
	$\frac{d\vec{r}}{dt}=\vec{v}$
	$\frac{d\vec{v}}{dt}+\frac{\mu}{r^3}\vec{r}=grad{R}$
	- solution without right hand side
		- $\vec{r}=\vec{r}(t,6 constants)$
		- $\vec{v}=\vec{v}(t,6 constants)$
			- 6. init. constants are 6 orbital elements: $\Omega,i,\omega,a,e,M$
			- variation of all 6 orbital elements: $\Omega(t),i(t),\omega(t),a(t),e(t),M(t)$
- Lagrange's planetary equations:
$$\frac{d\Omega}{dt}=\frac{1}{nabsini}\frac{\delta R}{\delta i}$$
$$\frac{dM}{dt}=-\frac{2}{na}\frac{\delta R}{\delta a}-\frac{b^2}{na^4e}\frac{\delta R}{\delta e}$$
$$\frac{de}{dt}=-\frac{b}{na^3e}\frac{\delta R}{\delta \omega}+\frac{b^2}{na^4e}\frac{\delta R}{\delta M}$$
$$\frac{di}{dt}=-\frac{1}{nab\sin{i}}\frac{\delta R}{\delta\Omega}+\frac{\cos{i}}{nab\sin{i}}\frac{\delta R}{\delta\omega}$$
$$\frac{da}{dt}=\frac{2}{na}\frac{\delta R}{\delta M}$$
$$\frac{d\omega}{dt}=-\frac{cos{i}}{nab\sin{i}}\frac{\delta R}{\delta i}+\frac{b}{na^3e}\frac{\delta R}{\delta e}$$
- Perturbative potential must be expressed by orbital
elements $\Omega,i,\omega,a,e,M$
- average value of R in one period T:
$$\overline{R}=-\frac{1}{4}\frac{\mu R^2}{a^3(1-e^2)^{\frac{3}{2}}}J_2(3\sin{^2i-2})$$
	-	je to funkcia f(a,e,i)
- Perturbative potential can be decomposed into average (secular) and periodic part: $R=R_s+R_p$
	- average value $R_p$ in one period is 0
	- $R_s=\overline{R}$

- Replacing perturbative potential by its secular part: $R$ => $R_s$ => $\overline{R}$
	- R=R(a,e,i)

- Lagrange’s planetary equations:
	- e,i,a are constants
$$\frac{de}{dt}=e(\frac{\delta R}{\delta\omega},\frac{\delta R}{\delta M})$$
$$\frac{di}{dt}=i(\frac{\delta R}{\delta\Omega},\frac{\delta R}{\delta\omega})$$
$$\frac{da}{dt}=a(\frac{\delta R}{\delta M})$$

$$\frac{d\Omega}{dt}=\Omega(\frac{\delta R}{\delta i}) => \frac{d\Omega}{dt}=-\frac{3}{2(1-e^2)^2}nJ_2(\frac{R}{a})^2\cos{i}$$

$$\frac{dM}{dt}=M(\frac{\delta R}{\delta a},\frac{\delta R}{\delta e}) => \frac{dM}{dt}=n+\frac{3}{4(1-e^2)^{\frac{3}{2}}}nJ_2(\frac{R}{a})^2(3\cos^2{i-1})$$

$$\frac{d\omega}{dt}=\omega(\frac{\delta R}{\delta i},\frac{\delta R}{\delta e}) => \frac{d\omega}{dt}=\frac{3}{4(1-e^2)^{2}}nJ_2(\frac{R}{a})^2(5\cos^2{i-1})$$

Sun‐synchronous orbits
- Earth rotates counterclockwise around the Sun with angular velocity 0.986° per day
- if satellite orbit rotates clockwise with the same angular velocity, position of orbit relative to the Sun will be still the same