<h1>Orbital Perturbations I.</h1>

<b>Content:</b>
- Disturbing Forces
- Geopotential

<h2>1. Disturbing Forces</h2>

Orbits of Earth satellites are influenced by 2 facts:
- The Earth is not exactly spherical and the mass distribution is not exactly spherically symmetric
- The satellite feels other forces apart from the Earth’s attraction:
	- attractive forces due to other heavenly bodies
	- forces that can be globally categorized as frictional
<b>All these influences are called perturbations</b>

In principle, these force effects can be divided into two
categories:
1. <b>conservative forces</b> - can be expressed in terms of potential
2. <b>non-conservative forces</b> - cannot be expressed in terms of potential

<b>Perturbing forces:</b>
- <b>Conservative forces</b>: can be derived from potential:
	- flattening of the Earth
	- Attraction of the Moon
	- Attraction of the Sun
	- Attraction by other planets

- <b>Non‐conservative forces</b>: cannot be derived from potential – dissipative forces:
	- atmospheric drag
	- radiation pressure

The influence of individual perturbation forces on the motion of a satellite is most often expressed in the form of acceleration, which corresponds to a given perturbation force.

<b>Influence of perturbing forces expressed by accelerations:</b>
- GM – attraction of Earth (sphere shape)
- J2 – flattening of the Earth (Earth ellipsoid)
- J4, J6 – potential of Earth expressed by higher orders
- Moon, Sun, Planets – their attraction

<h2>2. Geopotential</h2>

Potential of single mass point:
![[p7_potential.png]]
- potential energy of two mass system using relative distance $\vec{r}$:
$$E_p=-G\frac{m_1m_2}{r}=-\frac{\mu m_2}{r}$$
- gravitational potential:
$$U(\vec{r})=U(r)=-\frac{E_p}{m_2}=\frac{Gm_1}{r}=\frac{\mu}{r}$$
$$E_p=-U(r)m_2$$
- equation of motion expressed by potential:
$$\vec{\ddot{r}}=grad(U(r))$$
- Potencial energy of shell with sphere shape with radius R and thickness dR'
- mass of shell body is M, density of shell body is $\sigma$
![[p7_mass_density.png]]

- differential mass dM is defined by differential angles $d\phi$ and $d\theta$, its position is defined by angles $\phi$ and $\theta$
![[p7_angles.png]]

potential of differential mass $dM$ can be expressed: 
$E_p=-G\frac{m_1m_2}{r}$ => $dE_p=-G\frac{dMm_2}{p}$
![[p7_potential_mass.png]]

$$U(\vec{r})=U(r)=\int_{(M)}\frac{G}{p}dM=G\sigma dR'R^2\int_{0}^{2\pi}\int_{0}^{\pi}\frac{sin\theta}{p}d\theta d\phi$$

- law of cosines: 
$p=(r^2+R^2-2rRcos\theta)^\frac{1}{2}$ => $dp=rR\frac{sin\theta d\theta}{p}$

- dosadime do U(r): 
$$U(r)=G\sigma dR'R^2\int_{0}^{2\pi}\int_{r-R}^{r+R}\frac{1}{rR}dpd\phi$$
- po integracii:
$$U(r)=G4\pi\sigma dR'R^2\frac{1}{r}=\frac{GM}{r}$$

<b>1. approx. - sphere</b>
<b>2. approx. - ellipoid</b>
