<h1>Orbital maneuvers II.</h1>

<b>Content:</b>
- Interplanetary Hohmann transfers
- Randezvous opportunities
- Sphere of influence
- Method of patched conics
- Planetary Flyby

<h2>1. Interplanetary Hohmann transfers</h2>