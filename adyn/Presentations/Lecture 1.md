<h1>Introduction to Astrodynamics</h1>

<b>Content:</b>
- What is Astrodynamics?
- Historical Background
- Contents of Astrodynamics Course
- Mathematical Intro

<h2>What is Astrodynamics?</h2>

- simple definition: the study of controlled flight paths of man‐made spacecraft
- detailed definition: the determination, prediction, physical adjustment, and optimisation of trajectories in space; space navigation and mission analysis; perturbation theories and expansions; spacecraft attitude dynamics and estimation
- astrodynamics is sometime separeted from celestial mechanics, by limiting the scope of the latter to the motion of natural celestial bodies, which is not under human control.
- but the motion of all celestial bodies, natural or artificial, is governed by the same laws of mechanics.
- Celestial Mechanics – is the study of the natural motion of celestial bodies
- Astrodynamics – is the study of the controlled flight paths of spacecraft
- Orbital Mechanics – is the study of the principles governing the motion of bodies around other bodies under the influence of gravity and other forces
- Attitude Dynamics and Attitude Control – consider the spacecraft’s rotational motion about its center of mass.
- Spacecraft attitude dynamics – is the applied science whose aim is to understand and predict how the spacecraft’s orientation evolves.

<h2>Historical Background</h2>

- Copernicus (1473 ‐ 1543)
- Galileo (1546–1642)
- Kepler (1571–1630)
- Newton (1642–1727)
- Euler (1707–1783)
- Lagrange (1736–1813)
- Laplace (1749–1827)
- Gauss (1777–1855)

<h2>Contents of Astrodynamics Course</h2>

- Introduction to Astrodynamics
- Two body problem – motion in inertial frame
- Two body problem – relative motion
- Two body problem in 2D I.
- Two body problem in 2D II. (time and position)
- Orbits in 3D
- Orbital perturbations I.
- Orbital perturbations II.
- Orbital maneuvers I.
- Orbital maneuvers II.
- Satellite Attitude Dynamics I.
- Satellite Attitude Dynamics II.

<h2>Mathematical Intro</h2>

 - Vectors
 - Differential equations