<h1>week 5</h1>

<p>What is Processor?</p>

- digital system that receives instructions and data from surrounding and precesses the received and/or own internally stored data by a specific manner.
- the processing depends on the instruction, which is used - each instruction represents a specific algorithm
- the results of processing are returned to the processor surrounding

<p>Simplified Look at Processor</p>

- processor has a control unit and uses procedural style for processing of data and signals
- control unit is responsible controlling all processes of the processor
- operation part of processor receives the control commands from the control unit and executes the operation on data, it's in principle the ALU
- such a digital system has a structure called primary architecture
![[5_digisys.png]]
- DI, DO is data I/O, this I/O is complex in general - DI: Boolean N-bit vector, DO: pair of Boolean vectors
- C is a control input for OP; usually data type is: Boolean R-bit vector
- S is a status of Operation part; its a feedback from OP to CU; data similar to C
- ES, EC are external status and external control
- C and S are often called control and state signal

<b>Another Look at Processor</b>

![[5_cpu.png]]

<b>Instruction</b>

- two types of instructions:
	- signal instructions - non-structured
	- structured
- signal instructions contain only information about the prescribed operation with predefined algorithm, which is supposed to be performed with already implicitly specified data structures within internal data and data provided form outside using fixed places. Usually represented as a logic signal or set of such signals.
- example: RESET and INTERRUPT, which are assigned to logic inputs of system; or COMPLEMENT, which prescribes a bit-wise negation or Boolean vector saved in fixed location
- structured instruction contrain not only the prescribed operaiton that is supposed to be performed but pointers to data structures, which are used for the processing - these instruction are universal in a sense that they operate on any data that is selected by the instruction as well
- example: ADD instruction, which prescribes addition of two integer numbers and location of these data to be added are specified within the instruction as well

<b>Instruction Set Architecture</b>

- very popular in its abbreviation form as ISA
- ISA is set of instructions supported by CPU
- servers as a contract between harware world and software world
- computer program is compiled and translated to binary code for a particular ISA
- the binary code of program represents a sequence of binary-coded instructions that are available by the given ISA
- software developers are obliged to use only those instructions that are available by the ISA
- hardware developers are obliged to support the ISA instructions in the processor, using the instruction encoding described in ISA
- ISA does not specify how the processor is designed/implemented - there are usually many CPU implementations for the same instruction set architecture

<b>Active vs. Passive processor</b>

- processors can be split to two types according to the way the instructions are driven to processor
	- active processors
	- passive processors

<b>Active processors</b>

- control unit is designed in such a way that instructions are automatically extracted from external memory, one by one - this way the processor reads the program from instruction memory, which is typically a RAM
- they usually use mostly structured instructions - the non-structured instruction are usually only RESET and INTERRUPTs, the rest is usually structured
- they keep track of the information which instruction they are executing within the program at the moment - this information is used for reading the next instruction
- it's stored in program counter (PC)
- example: simplified active processor with memory-oriented architecture
- specification of processor (CPU):
![[5_activecpu.png]]

- input of instruction and data from surrounding and output data to surrounding:
- D is data I/O 32-bit wide; data type: data or instruction
- A is address O 30 bits wide; data type: address
- Rd/Wr is used to read instruction from main memory and read/write of data from or to surrounding
- active output signal for output Rd/Wr is:
	- reading instruction or reading data Rd/Wr = 1
	- Writing data Rd/Wr = 0
- Req is used when the processor requires to read instruciton or data or requires to write data
- arctive value of output signal req = 1
- Wait is a response from surrounding, informing about a need of waiting fo finishing instruction read or data read or finishing data write
- Res is used to reset CPU
- State variables of processor:
- MBR - Memory Bus Register; data type: instruction or data
- MAR - Memory Address Register; data type: address
- PC - Program Counter (instruction pointer)l data type: address, i.e.: integer number in interval of < 0, 2^30 - 1 )
- IR - Instruction Register; data type: instruction, i.e.:
	- Record
	- Opcode: symbol from set { LD, ST, ADD, BRN }
	- Address: integer number in interval < 0, 2^30-1)
		- operation + with module 2^30
 -	AC - Accumulator Register
	 -	Data type: data, i.e.: integer number in interval < -2^31, +2^31-1 ); + in 2's complement code
- Contr - Control register - represents status/state of CPU

<b>Gross Structure of Processor</b>

![[5_gross.png]]

- C - Control input of OP, driven by CU that controls the performing of the instruction cycle:
	1. Read (fetch) instruction from RAM from surrounding of CPU
	2. Execute instruction
- S - status input of CU, used by OP to inform CU about state of OP during execution of the instruction
- ES - external status input <b>Wait</b> from surrounding
- EC - external control output (<b>Rd/Wr</b> and <b>Req</b>) for surrounding
- there are <b>4 instructions</b> in our CPU example:
	- <b>Addition</b>: format = ADD, address;
		- performs addition of value in AC and content of external memory RAM at location specified with address "address", and the result is stored into AC. thus:
			- AC = AC + M[address]
	- <b>Load Data</b> (LD): format = LD, address;
		- a data from external RAM memory from location at address "address" or from port in I/O system with address "address" is moved to AC
			- AC = M[addres]
	- <b>Store</b> (save) data (ST): format = ST, address;
		- a data from AC is movedto external RAM memory to location at address "address" or to I/O port with address "address".
	- <b>Branch</b> (conditional jump) according to sign of value in AC (BRanch if Nechative => BRN):
		- format = BRN, address;
		- if the value of AC is negative, then PC is set to value "address", otherwise PC is set to PC + 1
	- We can see that these instructions are structured
	
	<b>CISC vs. RISC</b>
	
	- Instruction set architectures of active processors with structured instructions are divided to two classes:
		1. <b>Complex Instruction Set Computing</b>; popoular under abbreviation CISC; for example processors belonging to x86 family, e.g. IBM, Intel or AMD
		2. <b>Reduced Instruction Set Computing</b>; popular under abbreviation RISC; for example ARM, MIPS or RISC-V processors

<b>Passive Processors</b>

- unlike active processors, these receive instructions passively
- they don't read binary code of program from surrounding automatically
- they can be designed to <b>address data structures in external</b> memory and move data to processor as well as store dat structures to this memory
- example: arithmetic co-processor

<b>Processor Types According to Applications</b>

- According to applications, we can divide processors to <b>universal</b> and <b>application-specific</b>
- <b>universal</b> are the basic part of computers, operating as CPU. They are designed to process various algorithms. These systems are nowadays produced as standard integrated circuits (IC) with very large scale integration (VLSI) or as system on chip (SoC)
- <b>Applicaiton-specific processors</b> are processors that are designed for special functionality. They are either produced as IC or offered as intellectual property (IP) cores - e.g. processors for signal processing (signal processors), graphical cores or arithmetic co-rpocessors, DMA processors, etc. or custom-made ASIC

<b>System as a Composition of Subsystems</b>

- Complex and extensive systems are needed to be viewed as a composition of simpler subsystems communicating with each other, which represent:
	- <b>final subsystems</b> (library modules)
	- elements, <b>subsystems</b> that are not yet done and wil be designed and implemented for the given use case
- Design of such a composition belongs to the system level of HW design, usually it represents the top-level module. Nowadays the whole system can be implemented on single chip (ASIC or FPGA) with hundreds of millions of transistors (system on chip - SoC)

<b>System as a Composition of Subsystems</b>

1. Embedding module inside another module
2. Parallel composition of modules
3. Sequential composition of modules

<b>Communication between Processors</b>

- Data exchange using common memory
- Data exchange using message sending
- Communication channel can be:
	- special processor as DMA (direct memory access) unit
	- buffer memory (FIFO buffers)