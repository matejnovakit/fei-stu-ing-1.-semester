<h1>week 4</h1>

<b>Combining `if` conditions</b>
```verilog
always @(posedge clk) begin
	abc <= 4'b0001;
	if (cond1) begin
		abc <= 4'b0010;
	end
	if (cond2) begin
		abc <= 4'b0100;
	end
	if (cond3) begin
		abc <= 4'b1000;
	end
end
```
<b>Conditions usign `case`</b>
- similar as `if` condition
- `if` has only 2 branches (true branch and false branch)
- `if` is implemented as a multiplexer
- tree of `if`-s has explicitly specified priority of evaluation according to the order of `if`-s in the code
- `case` has ny number of branches, which have the same priority
	- branches are equivalent
	- combinational logic is balanced from the point of view of critical path length
- Also used for decoding of signal
	- decoding of instruction of procesor
```verilog
case (condition)
	value1 : begin
		code....
	end
	value2 : begin
		code...
	end
	default : begin
	end
endcase
```
<b>Finite state machines (FSM)</b>
- Mealy or Moore
- used to represent control logic:
![[4_mealy.png]]
- example:
	- states: S0, S1, S2, S3
	- input condition / output
![[4_machine.png]]
![[4_fsm.png]]

<b>Comments</b>
- better readability of code
```verilog
// one line
/**/ multi line
```
<b>Parameters</b>
- Parameters server as constants typically used to specify bit_width of vectors and duration of time delays
- increases clarity and reusability of code
- keyword <i>parameter</i>
- defined within selected module
- is valid within an instance of the module
- possibility to rewrite parameters of instance during creationg of the instance
- content of parameter is a psecific value or another parameter
- example
```verilog
module sp_ram #(
	parameter DATA_WIDTH = 8,
	parameter ADDR_WIDTH = 8,
	parameter RAM_DEPTH = 1 << ADDR_WIDTH
)(
	input wire clk,
	input wire [ADDR_WIDTH-1:0] addr,
	input wire [DATA_WIDTH-1:0] data_in,
	input wire ce,
	input wire we,
	output wire [DATA_WIDTH-1:0] data_out
);
// Actual code of RAM here...
endmodule
```
<b>Instantiation of Parametrizable Module</b>
```verilog
module sp_ram #(
	.DATA_WIDTH (16),
	.ADDR_WIDTH (32)
) sp_ram_1 (
	.clk		(clk),
	.addr		(addr_1),
	.data_in	(data_in_1),
	.ce			(ce_1),
	.we			(we_1),
	.data_out	(data_out_1)
);
```
<b>Generate</b>
- keyword `generate` is used to conditional or iterative generation of some piece of code
	- always block, assign statements or istance of module
- very suitable for description of parametrizable module, where parameters define also the structure of the module
- we can use support variables with `genvar`

<b>Conditional Generation</b>
```verilog
generate
	if(ADDR_W == 0) begin
		assign d_out = 32'0;
	end
	if((ADDR_W > 0) && (ADDR_W < N)) begin
		assign d_out = ABC[ADDR_W-1:0];
	end // else is NOT allowed
	if(ADDR_W == N) begin
		assign d_out = ABC - 1'b1;
	end
endgenerate
```

<b>Cycles `for` and `while`</b>
- they express repeating the some functionality
- the same as in C language
- sequential way of description of parallel logic
- be careful with the complexity of the fenerated hardware
- `for` cycle
```verilog
for (i = 0; i < N; i = i + 1) begin
	// repeated code, e.g. shr[i+1] = shr[i];
end
```
- `while` cycle
```verilog
i = 0;
while (i < N) begin
	//repeated code
	i = i + 1;
end
```

<b>Cycle in generate</b>
- normal cycle must be placed into `always` block
- cycle inside `generate` block can be used to create several instances according to parameter
- example:
```verilog
genvar i;
generate
	for(i = 0; i<N;i=i+1) begin : for_loop_in_gen
		sp_ram #(
			.DATA_WIDTH (16+i),
			.ADDR_WIDTH (8+N-i)
		) sp_ram_i(
		...
		)
	end
endgenerate
```